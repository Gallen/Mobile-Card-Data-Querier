import Vue from 'vue';

export default {
  getUnicomCardInfo (card_id) {
    const url = `http://appservice.duapp.com/unicomcard/${card_id}`;
    return Vue.http.get(url);
  },
};
