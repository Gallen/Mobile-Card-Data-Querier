import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import router from './router';

// install router
Vue.use(VueRouter);
Vue.use(VueResource);

const App = Vue.extend({
  components: require('./App.vue')
});

router.start(App, 'body');

// just for debugging
window.router = router;
