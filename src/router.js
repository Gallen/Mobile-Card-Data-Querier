import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
const router = new VueRouter({
  // history: true,
  saveScrollPosition: true
});

router.map({
  '/index': {
    component: require('./components/chinaUnicom.vue')
  },
});

router.redirect({
  '*': '/index'
});

router.afterEach(function(transition) {
  //...
});

export default router;
